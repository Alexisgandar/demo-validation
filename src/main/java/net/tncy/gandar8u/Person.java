package net.tncy.gandar8u;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Person {
	@NotNull
	@Size(min=1)
	private String firstName;
	@NotNull
	@Size(min=1)
	private String lastName;
	@NotNull(groups = {AdultCheck.class})
	private Date birthDate;
	private String cityzenship;
	@NotNull(groups = {AdultCheck.class})
	@Min(value=18, groups = {AdultCheck.class})
	private transient Integer age;
	
	public Person(String firstName, String lastName, Date birthDate, String cityzenship) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.cityzenship = cityzenship;
		this.computeAge();
	}
	
	private void computeAge() {
		if(birthDate != null) {

		}
	}
	
}
