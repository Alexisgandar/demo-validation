package net.tncy.gandar8u;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

public class PersonTest {
	private static Validator validator;
	
	@BeforeClass
	public static void setUpClass() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void validateWelldefinedPerson() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(1974, calendar.DECEMBER,3);
		Date birthDate = calendar.getTime();
		Person p = new Person("xavier","Roy",birthDate, "FR");
		Set <ConstraintViolation<Person>> constraintViolation = validator.validate(p,AdultCheck.class);
		assertEquals(0, constraintViolation.size());
	}


}
